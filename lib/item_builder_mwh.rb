# frozen_string_literal: true

require 'item_builder_mwh/version'
require 'item_builder_mwh/modes'
require 'item_builder_mwh/modes/base_service'
require 'item_builder_mwh/modes/price_service'
require 'item_builder_mwh/modes/quantity_service'
require 'item_builder_mwh/modes/simple_service'
require 'item_builder_mwh/modes/create_service'
require 'item_builder_mwh/modes/map_create_service'
require 'item_builder_mwh/modes/precondition_create_service'
require 'item_models'
require 'item_builder_mwh/zilingo_quantity_service'
require 'item_builder_mwh/lazada_quantity_service'
require 'item_builder_mwh/zalora_quantity_service'
require 'item_builder_mwh/shopify_quantity_service'

class ItemBuilderMwh
  attr_reader :listing_ids
  attr_reader :listings
  attr_reader :mode
  attr_reader :wh_spaces
  attr_reader :variants
  attr_reader :variant_ids
  def initialize(listing_ids, mode)
    @listing_ids = listing_ids
    @mode = mode
  end

  def self.build(listing_ids, mode)
    new(listing_ids, mode).mode_check
  end

  def mode_check
    if mode == :quantity || mode == :simple || mode == :active
      quantity_simple_mode
    elsif mode == :create || mode == :item_create || mode == :precondition_create || mode == :precondition_update || mode == :update
      create_mode
    elsif mode == :map_create
      map_create_mode
    else
      default
    end
  end

  def quantity_simple_mode
    listings.map do |listing|
      next unless listing.local_id.present?

      param =
        if listing.channel_id == 18
          qty_simple_params(listing)
            .merge(zilingo_quantity: zilingo_quantity)
        elsif listing.channel_id == 13
          qty_simple_params(listing)
            .merge(zalora_reserved_stock: zalora_reserved_stock)
        elsif listing.channel_id == 2
          qty_simple_params(listing)
            .merge({shopify_inventory_location: shopify_inventory_location})
        elsif listing.channel_id == 3
          qty_simple_params(listing)
            .merge({lazada_quantity: lazada_quantity})
        else
          qty_simple_params(listing)
        end
      modes[mode].new(param).perform
    end.compact
  end

  def default
    datas =
      if mode == :precondition_create
        VariantListing.where(
          channel_association_id: channel_association_ids,
          profile_channel_association_id: account_id
        )
      else
        listings
      end

    datas.map do |listing|
      if listing.local_id.present?
        modes[mode].new(listing: listing).perform
      end
    end
  end

  def qty_simple_params(listing)
    {
      listing: listing, wh_spaces: wh_spaces, variants: variants,
      stock_allocs: stock_allocs, variant_listings: variant_listings,
      bundles: bundles, item_bundle_variants: item_bundle_variants,
      existing_alloc_stocks: existing_alloc_stocks,
      reserved_stocks: reserved_stocks
    }
  end

  def create_mode
    item_listings.map do |listing|
      param =
        if listing.channel_id == 12
          qty_simple_params(listing)
            .merge(shipping_providers: shipping_providers)
        else
          qty_simple_params(listing)
        end
      modes[mode].new(param.merge(mode: mode)).perform
    end
  end

  def map_create_mode
    listings.map do |listing|
      modes[mode].new(qty_simple_params(listing)).perform
    end
  end

  def existing_alloc_stocks
    @existing_alloc_stocks ||= VariantListingStockAllocation.where(
      variant_association_id: vl_ids
    ).where('end_at >= NOW()')
  end

  def vl_ids
    @vl_ids ||= variant_listings.map(&:id).uniq
  end

  def variant_listings
    @variant_listings ||= VariantListing.where(variant_id: variant_ids)
  end

  def item_bundle_variants
    @item_bundle_variants ||= BundleVariant.where(
      bundle_id: bundle_ids
    )
  end

  def bundle_ids
    @bundle_ids ||= bundles.map(&:id).uniq
  end

  def bundles
    @bundles ||= Bundle.where(variant_id: variant_ids).group(:variant_id)
  end

  def stock_allocs
    @stock_allocs ||= VariantListingStockAllocation.where(
      variant_association_id: vl_ids
    )
  end

  def wh_spaces
    @wh_spaces ||= WarehouseSpace.where(item_variant_id: variant_ids).where.not(warehouse_id: nil)
  end

  def variants
    @variants ||= Variant.where(id: variant_ids)
  end

  def variant_ids
    @variant_ids ||= listings.map(&:variant_id).uniq
  end

  def skus
    @skus ||= listings.map(&:local_id).uniq
  end

  def channel_association_ids
    @listing_item_ids ||= listings.map(&:channel_association_id).uniq
  end

  def zilingo_quantity
    @zilingo_quantity ||= ItemBuilderMwh::ZilingoQuantityService.new(
      listings: listings, skus: skus
    ).perform
  end

  def zalora_reserved_stock
    @zalora_reserved_stock ||= ItemBuilderMwh::ZaloraQuantityService.new(
      listings: listings, skus: skus
    ).perform
  end

  def shopify_inventory_location
    @shopify_inventory_location ||= ItemBuilderMwh::ShopifyQuantityService.new(
      listings: listings, skus: skus
    ).perform
  end

  def lazada_quantity
    @lazada_quantity ||= nil
  end

  def order_host
    url = ENV['ORDERS_URL'] || 'orders.forstok.com'
    url + '/api/v3/item_line/reserved_stock'
  end

  def reserved_params
    "account_id=#{account_id}
    &item_variant_ids=#{variant_ids.join(',')}"
  end

  def reserved_stocks
    @reserved_stocks ||= JSON.parse(RestClient.get(
      "#{order_host}?#{reserved_params}"
    ).body) if [3,13].include?(listings[0].channel_id)
  end

  def modes
    {
      price: ItemBuilderMwh::Modes::PriceService,
      quantity: ItemBuilderMwh::Modes::QuantityService,
      simple: ItemBuilderMwh::Modes::SimpleService,
      active: ItemBuilderMwh::Modes::ActiveService,
      create: ItemBuilderMwh::Modes::CreateService,
      map_create: ItemBuilderMwh::Modes::MapCreateService,
      precondition_create: ItemBuilderMwh::Modes::CreateService,
      precondition_update: ItemBuilderMwh::Modes::CreateService,
      item_create: ItemBuilderMwh::Modes::CreateService,
      update: ItemBuilderMwh::Modes::CreateService,
    }
  end

  def listings
    @listings ||= VariantListing.joins(:variant).where(id: listing_ids)
  end

  def account_id
    account_id ||= listings[0].profile_channel_association_id
  end

  def item_listings
    @item_listings ||= VariantListing.joins(:variant).includes(:item_listing).where(
      id: listing_ids
    ).group(:channel_association_id)
  end

  def shipping_providers
    @shipping_providers ||= ShopeeShippingProvider.where(
      enabled: true, mask_channel_id: 0, account_id: account_id
    )
  end
end
