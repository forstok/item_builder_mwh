# frozen_string_literal: true

require 'item_builder_mwh/modes/base_service'
require 'item_builder_mwh/modes/price_service'
require 'item_builder_mwh/modes/quantity_service'
require 'item_builder_mwh/modes/simple_service'
require 'item_builder_mwh/modes/active_service'
class ItemBuilderMwh
  module Modes
    attr_reader :listing
    attr_reader :wh_spaces
    attr_reader :variants
    attr_reader :stock_allocs
    attr_reader :bundles
    attr_reader :item_bundle_variants
    attr_reader :existing_alloc_stocks
    attr_reader :variant_listings
    attr_reader :reserved_stocks
    attr_reader :zilingo_quantity
    attr_reader :lazada_quantity
    attr_reader :zalora_reserved_stock
    attr_reader :shopify_inventory_location
    attr_reader :shipping_providers
    def initialize(args)
      @listing = args.fetch(:listing)
      @wh_spaces = args.fetch(:wh_spaces, [])
      @variants = args.fetch(:variants, [])
      @stock_allocs = args.fetch(:stock_allocs, [])
      @bundles = args.fetch(:bundles, [])
      @item_bundle_variants = args.fetch(:item_bundle_variants, [])
      @existing_alloc_stocks = args.fetch(:existing_alloc_stocks, [])
      @variant_listings = args.fetch(:variant_listings, [])
      @reserved_stocks = args.fetch(:reserved_stocks, [])
      @zilingo_quantity = args.fetch(:zilingo_quantity, [])
      @lazada_quantity = args.fetch(:lazada_quantity, [])
      @zalora_reserved_stock = args.fetch(:zalora_reserved_stock, [])
      @shopify_inventory_location = args.fetch(:shopify_inventory_location, {})
      mode = args.fetch(:mode, '')
      @shipping_providers = args.fetch(:shipping_providers, [])
      warehouse unless mode == :create || mode == :item_create || mode == :precondition_create || mode == :precondition_update || mode == :update
    end

    def base
      {
        id: listing.id,
        local_id: listing.local_id,
        local_item_id: listing.local_item_id,
        sku: listing.sku,
        variant_id: listing.variant_id
      }
    end

    def existing_allocated_stock
      existing_alloc_stocks.map do |els|
        if listings.pluck(:id).include?(els.variant_association_id)
          els
        end
      end.compact
    end

    def listings
      variant_listings.select {|vl| vl.variant_id == listing.variant_id}
    end

    def bundle_variants
      if bundle.present?
        item_bundle_variants.select {|ibv| ibv.bundle_id == bundle.id }
      end
    end

    def bundle
      bundles.select {|b| b.variant_id == listing.variant_id }.first
    end

    def stock_alloc
      stock_allocs.select {|sa| sa.variant_association_id == listing.id }.first
    end

    def variant
      variants.select {|v| v.id == listing.variant_id }.first
    end

    def warehouse
      if multiwarehouse && wh_routing == 1
        warehouses << to_h(warehouse_spaces.first)
      else
        warehouse_list
      end
    end

    def warehouse_list
      if warehouse_spaces.present?
        warehouse_spaces.each do |warehouse_space|
          warehouses << to_h(warehouse_space)
        end
      else
        warehouses << to_h(nil)
      end
    end

    def warehouse_spaces
      WarehouseSpace
        .joins('JOIN warehouse_mappings ON
          warehouse_spaces.warehouse_id = warehouse_mappings.warehouse_id')
        .joins('JOIN warehouses ON
          warehouse_spaces.warehouse_id = warehouses.id')
        .where("warehouse_mappings.profile_channel_association_id=#{pca}")
        .where('warehouse_mappings.deleted_at is NULL')
        .where("warehouse_spaces.item_variant_id=#{variant_id}")
        .where('warehouses.consignment = 0')
    end

    def pca
      listing.profile_channel_association_id.to_s
    end

    def variant_id
      bundle_variants&.first&.variant_id || listing.variant_id
    end

    def warehouse_mapping(warehouse_id)
      WarehouseMapping
        .where(profile_channel_association_id:
          listing.profile_channel_association_id,
               warehouse_id: warehouse_id)
    end

    def wh_mapping(warehouse_id)
      data = warehouse_mapping(warehouse_id)
      return nil if data.empty?

      warehouse_mapping(warehouse_id)[0].channel_warehouse_id
    end

    def credential
      return @shopify_inventory_location['credential'] if @shopify_inventory_location['credential'].present?

      nil
    end

    def multiwarehouse
      return credential['multiwarehouse'] if credential.present?

      false
    end

    def wh_routing
      return credential['warehouse_routing'] if credential.present? && credential['warehouse_routing'].present?

      0
    end

    def warehouses
      @warehouses ||= []
    end
  end
end
