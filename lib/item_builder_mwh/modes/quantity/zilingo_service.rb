# frozen_string_literal: true

require 'item_builder_mwh/modes/quantity/base'
class ItemBuilderMwh
  module Modes
    module Quantity
      class ZilingoService < Base
        def perform
          available_quantity - local_qty
        end
      end
    end
  end
end
