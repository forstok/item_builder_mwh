# frozen_string_literal: true

require 'item_builder_mwh/modes.rb'
require 'warehouse_models'
require 'item_builder_mwh/get_quantity_service'
require 'item_builder_mwh/modes/map_create/shopee_service'
require 'item_builder_mwh/modes/map_create/tokopedia_service'
require 'item_builder_mwh/modes/quantity/base'
require 'item_builder_mwh/modes/quantity/lazada_service'
require 'item_builder_mwh/modes/quantity/zalora_service'
require 'item_builder_mwh/modes/quantity/zilingo_service'
require 'item_builder_mwh/modes/price/base'
require 'item_builder_mwh/modes/price/blibli_service'
require 'item_builder_mwh/modes/price/bukalapak_service'
require 'item_builder_mwh/modes/price/zalora_service'
require 'item_builder_mwh/modes/price/shopify_service'
require 'item_builder_mwh/modes/price/sale_price_policy'
require 'item_builder_mwh/modes/price/jd_service'
class ItemBuilderMwh
  module Modes
    class MapCreateService
      include Modes

      MAP_CREATE_CHANNEL = {}.tap do |hash|
        hash[12]      = :Shopee
        hash[15]      = :Tokopedia
      end.freeze

      def perform
        base.merge!(warehouse: warehouses).merge(map_create).merge(map_create_channel)
      end

      def map_create
        {
          name: listing.name,
          description: listing.description,
          weight: listing.package_weight,
          length: listing.package_length,
          width: listing.package_width,
          height: listing.package_height,
          condition: listing.new_condition,
          option_name: listing.option_name,
          option_value: listing.option_value,
          option2_name: listing.option2_name,
          option2_value: listing.option2_value,
          internal_data: listing.internal_data,
          images: images
        }
      end

      def images
        listing.item_listing_variant_images.map do |img|
          {
            id: img.id,
            local_id: img.local_id,
            local_url: img.local_url,
            url: img.image.image.url(:xlarge, timestamp: false)
          }
        end
      end

      def to_h(warehouse_space)
        {
          quantity: warehouse_space.present? ? real_quantity(warehouse_space) : 0,
          warehouse_id: warehouse_space.present? ? wh_mapping(
            warehouse_space.warehouse_id
          ) : ''
        }.merge(price)
      end

      def map_create_channel
        class_name = "ItemBuilderMwh::Modes::MapCreate::#{channel_name}Service"
        create_channel_service = class_name.constantize
        create_channel_service.new(listing).perform
      end

      def channel_name
        MAP_CREATE_CHANNEL[listing.channel_id].to_s
      end

      # Get Quantity Data

      QUANTITY_CHANNEL = {}.tap do |hash|
        hash[2]       = :Shopify
        hash[3]       = :Lazada
        hash[13]      = :Zalora
        hash[18]      = :Zilingo
      end.freeze

      def real_quantity(warehouse_space)
        if quantity_channel_name == 'Zilingo'
          qty(warehouse_space)
        else
          [qty(warehouse_space), 0].sort[1]
        end
      end

      def qty(warehouse_space)
        if quantity_channel_name.empty? || quantity_channel_name == "Shopify"
          available_quantity(warehouse_space)
        else
          qty_channel(warehouse_space)
        end
      end

      def qty_channel(warehouse_space)
        class_name = "ItemBuilderMwh::Modes::Quantity::#{quantity_channel_name}Service"
        qty_channel_service = class_name.constantize
        qty_channel_service.new(listing, available_quantity(warehouse_space), local_qty.to_i).perform
      end

      def quantity_channel_name
        QUANTITY_CHANNEL[listing.channel_id].to_s
      end

      def local_qty
        if quantity_channel_name == 'Zilingo'
          return 0 if zilingo_quantity.blank?

          zilingo_quantity[listing.local_id].to_i
        elsif quantity_channel_name == 'Zalora'
          return 0 if zalora_reserved_stock.blank?

          zalora_reserved_stock[listing.local_id].to_i
        else
          return 0 if lazada_quantity.blank?

          lazada_quantity[listing.local_id].to_i
        end
      end

      def credential
        return shopify_inventory_location['credential'] if shopify_inventory_location['credential'].present?

        nil
      end

      def wh_routing
        return credential['warehouse_routing'] if credential.present? && credential['warehouse_routing'].present?

        0
      end

      def available_quantity(warehouse_space)
        ItemBuilderMwh::GetQuantityService.new(
          listing: listing, stock_alloc: stock_alloc,
          variant: variant, bundle_variants: bundle_variants,
          existing_allocated_stock: existing_allocated_stock,
          listing_wh_sp_quantity: warehouse_space.quantity, wh_routing: wh_routing,
          selected_warehouse_space: warehouse_space
        ).perform
      end

      # Get Price

      PRICE_CHANNEL = {}.tap do |hash|
        hash[2]       = :Shopify
        hash[9]       = :Blibli
        hash[11]      = :Bukalapak
        hash[13]      = :Zalora
        hash[16]      = :Jd
      end.freeze

      def price
        if price_channel_name.empty?
          {
            price: listing.price,
            sale_price: listing.sale_price,
            sale_start_at: listing.sale_start_at,
            sale_end_at: listing.sale_end_at
          }
        else
          price_channel
        end
      end

      def price_channel
        class_name = "ItemBuilderMwh::Modes::Price::#{price_channel_name}Service"
        price_channel_service = class_name.constantize
        price_channel_service.new(listing).perform
      end

      def price_channel_name
        PRICE_CHANNEL[listing.channel_id].to_s
      end
    end
  end
end
