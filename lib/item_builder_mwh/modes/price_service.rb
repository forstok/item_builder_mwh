# frozen_string_literal: true

require 'item_builder_mwh/modes.rb'
require 'item_builder_mwh/modes/price/base'
require 'item_builder_mwh/modes/price/blibli_service'
require 'item_builder_mwh/modes/price/bukalapak_service'
require 'item_builder_mwh/modes/price/zalora_service'
require 'item_builder_mwh/modes/price/shopify_service'
require 'item_builder_mwh/modes/price/sale_price_policy'
require 'item_builder_mwh/modes/price/jd_service'
class ItemBuilderMwh
  module Modes
    class PriceService
      include Modes

      PRICE_CHANNEL = {}.tap do |hash|
        hash[2]       = :Shopify
        hash[9]       = :Blibli
        hash[11]      = :Bukalapak
        hash[13]      = :Zalora
        hash[16]      = :Jd
      end.freeze

      def perform
        base.merge!(
          warehouse: warehouses
        )
      end

      def to_h(warehouse_space)
        warehouse_ids(warehouse_space).merge(
          price
        )
      end

      def warehouse_ids(warehouse_space)
        {
          warehouse_id: warehouse_space.present? ? wh_mapping(
            warehouse_space.warehouse_id
          ) : ''
        }
      end

      def price
        if channel_name.empty?
          {
            price: listing.price,
            sale_price: listing.sale_price,
            sale_start_at: listing.sale_start_at,
            sale_end_at: listing.sale_end_at
          }
        else
          price_channel
        end
      end

      def price_channel
        class_name = "ItemBuilderMwh::Modes::Price::#{channel_name}Service"
        price_channel_service = class_name.constantize
        price_channel_service.new(listing).perform
      end

      def channel_name
        PRICE_CHANNEL[listing.channel_id].to_s
      end
    end
  end
end
