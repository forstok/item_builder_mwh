# frozen_string_literal: true

require 'warehouse_models'
require 'item_builder_mwh/modes.rb'
require 'item_builder_mwh/modes/simple/blibli_service'
require 'item_builder_mwh/modes/simple/bukalapak_service'
require 'item_builder_mwh/modes/simple/zalora_service'
require 'item_builder_mwh/modes/simple/shopify_service'
require 'item_builder_mwh/modes/simple/jd_service'
require 'item_builder_mwh/modes/simple/lazada_service'
require 'item_builder_mwh/modes/simple/shopee_service'
class ItemBuilderMwh
  module Modes
    class SimpleService
      include Modes

      QUANTITY_CHANNEL = {}.tap do |hash|
        hash[3]       = :Lazada
        hash[13]      = :Zalora
      end.freeze

      PRICE_CHANNEL = {}.tap do |hash|
        hash[2]       = :Shopify
        hash[9]       = :Blibli
        hash[11]      = :Bukalapak
        hash[13]      = :Zalora
        hash[16]      = :Jd
      end.freeze

      def perform
        base.merge!(
          warehouse: warehouses
        )
      end

      def to_h(warehouse_space)
        warehouse_ids(warehouse_space).merge(
          simple(warehouse_space)
        )
      end

      def warehouse_ids(warehouse_space)
        {
          warehouse_id: warehouse_space.present? ? wh_mapping(
            warehouse_space.warehouse_id
          ) : ''
        }
      end

      def simple(warehouse_space)
        qty = warehouse_space&.quantity || 0
        qty(qty).merge(
          price
        )
      end

      def qty(qty)
        if quantity_name.empty?
          {
            quantity: available_quantity(qty)
          }
        else
          {
            quantity: qty_channel(qty)
          }
        end
      end

      def price
        if price_name.empty?
          {
            price: listing.price,
            sale_price: listing.sale_price,
            sale_start_at: listing.sale_start_at,
            sale_end_at: listing.sale_end_at
          }
        else
          price_channel
        end
      end

      def price_channel
        class_name = "ItemBuilderMwh::Modes::Price::#{price_name}Service"
        simple_channel_service = class_name.constantize
        simple_channel_service.new(listing).perform
      end

      def qty_channel(qty)
        class_name = "ItemBuilderMwh::Modes::Quantity::#{quantity_name}Service"
        qty_channel_service = class_name.constantize
        qty_channel_service.new(listing, available_quantity(qty), local_qty.to_i).perform
      end

      def local_qty
        if quantity_name == 'Zilingo'
          return 0 if zilingo_quantity.blank?

          zilingo_quantity[listing.local_id].to_i
        elsif quantity_name == 'Zalora'
          return 0 if zalora_reserved_stock.blank?

          zalora_reserved_stock[listing.local_id].to_i
        else
          reserved_stocks.find {|rs| rs['variant_id'] == listing.variant_id }['reserved_quantity']
        end
      end

      def available_quantity(qty)
        ItemBuilderMwh::GetQuantityService.new(
          listing: listing, stock_alloc: stock_alloc,
          variant: variant, bundle_variants: bundle_variants,
          existing_allocated_stock: existing_allocated_stock,
          listing_wh_sp_quantity: qty
        ).perform
      end

      def price_name
        PRICE_CHANNEL[listing.channel_id].to_s
      end

      def quantity_name
        QUANTITY_CHANNEL[listing.channel_id].to_s
      end
    end
  end
end
