# frozen_string_literal: true

class ItemBuilderMwh
  module Modes
    module MapCreate
      class Base
        attr_reader :listing

        def initialize(listing)
          raise 'ilisting is not set' if listing.nil?

          @listing = listing
        end
      end
    end
  end
end
