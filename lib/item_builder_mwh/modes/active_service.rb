# frozen_string_literal: true

require 'item_builder_mwh/modes.rb'
class ItemBuilderMwh
  module Modes
    class ActiveService
      include Modes

      def perform
        base.merge!(
          warehouse: warehouses
        )
      end

      def to_h(warehouse_space)
        {
          active: listing.active,
          quantity: warehouse_space&.quantity || 0,
          warehouse_id: warehouse_space.present? ? wh_mapping(
            warehouse_space.warehouse_id
          ) : ''
        }
      end
    end
  end
end
