# frozen_string_literal: true

class ItemBuilderMwh
  module Modes
    module PreconditionCreate
      class Base
        attr_reader :ilisting

        def initialize(ilisting)
          raise 'ilisting is not set' if ilisting.nil?

          @ilisting = ilisting
        end
      end
    end
  end
end
