# frozen_string_literal: true

require 'date'
require 'item_builder_mwh/modes.rb'
require 'item_builder_mwh/modes/price_service'
require 'item_builder_mwh/modes/price/sale_price_policy'
class ItemBuilderMwh
  module Modes
    module Price
      class Base
        attr_reader :listing

        def initialize(listing)
          raise 'listing is not set' if listing.nil?

          @listing = listing
        end

        def sale_price_policy
          @sale_price_policy ||=
            ItemBuilderMwh::Modes::Price::SalePricePolicy.new(listing: listing)
        end
      end
    end
  end
end
