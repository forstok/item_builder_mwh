# frozen_string_literal: true

require 'date'
require 'item_builder_mwh/modes.rb'
require 'item_builder_mwh/modes/price/base'
require 'item_builder_mwh/modes/price/sale_price_policy'
class ItemBuilderMwh
  module Modes
    module Price
      class ShopifyService < Base
        def perform
          {
            price: listing.price,
            sale_price: sale_price,
            sale_start_at: listing.sale_start_at,
            sale_end_at: listing.sale_end_at
          }
        end

        private

        def sale_price
          sale_price_policy.on_sale? ? listing.sale_price : listing.price
        end
      end
    end
  end
end
