# frozen_string_literal: true

require 'date'
require 'item_builder_mwh/modes.rb'
require 'item_builder_mwh/modes/price/base'
class ItemBuilderMwh
  module Modes
    module Price
      class ZaloraService < Base
        def perform
          {
            price: listing.price,
            sale_price: sale_price_policy.sale_price,
            sale_start_at: listing.sale_start_at,
            sale_end_at: listing.sale_end_at
          }
        end
      end
    end
  end
end
