# frozen_string_literal: true

require 'warehouse_models'
require 'item_builder_mwh/modes.rb'
require 'item_builder_mwh/get_quantity_service'
require 'item_builder_mwh/modes/quantity/base'
require 'item_builder_mwh/modes/quantity/lazada_service'
require 'item_builder_mwh/modes/quantity/zalora_service'
require 'item_builder_mwh/modes/quantity/zilingo_service'

class ItemBuilderMwh
  module Modes
    class QuantityService
      include Modes

      QUANTITY_CHANNEL = {}.tap do |hash|
        hash[2]       = :Shopify
        hash[3]       = :Lazada
        hash[13]      = :Zalora
        hash[18]      = :Zilingo
      end.freeze

      def perform
        if channel_name == "Shopify"
          dataSIL = shopify_inventory_location[listing.local_id]

          base.merge!(dataSIL, warehouse: warehouses ) if dataSIL.present?
        else
          base.merge!(
            warehouse: warehouses
          )
        end
      end

      def to_h(warehouse_space)
        {
          id: warehouse_space&.warehouse_id,
          quantity: warehouse_space.present? ? real_quantity(warehouse_space) : 0,
          warehouse_id: warehouse_space.present? ? wh_mapping(
            warehouse_space.warehouse_id
          ) : ''
        }
      end

      def real_quantity(warehouse_space)
        if channel_name == 'Zilingo'
          qty(warehouse_space)
        else
          [qty(warehouse_space), 0].sort[1]
        end
      end

      def qty(warehouse_space)
        if channel_name.empty? || channel_name == "Shopify"
          available_quantity(warehouse_space)
        else
          qty_channel(warehouse_space)
        end
      end

      def qty_channel(warehouse_space)
        class_name = "ItemBuilderMwh::Modes::Quantity::#{channel_name}Service"
        qty_channel_service = class_name.constantize
        qty_channel_service.new(listing, available_quantity(warehouse_space), local_qty.to_i).perform
      end

      def channel_name
        QUANTITY_CHANNEL[listing.channel_id].to_s
      end

      def local_qty
        if channel_name == 'Zilingo'
          return 0 if zilingo_quantity.blank?

          zilingo_quantity[listing.local_id].to_i
        elsif channel_name == 'Zalora'
          return 0 if zalora_reserved_stock.blank?

          zalora_reserved_stock[listing.local_id].to_i
        else
          return 0 if lazada_quantity.blank?

          lazada_quantity[listing.local_id].to_i
        end
      end

      def credential
        return shopify_inventory_location['credential'] if shopify_inventory_location['credential'].present?

        nil
      end

      def wh_routing
        return credential['warehouse_routing'] if credential.present? && credential['warehouse_routing'].present?

        0
      end

      def available_quantity(warehouse_space)
        ItemBuilderMwh::GetQuantityService.new(
          listing: listing, stock_alloc: stock_alloc,
          variant: variant, bundle_variants: bundle_variants,
          existing_allocated_stock: existing_allocated_stock,
          listing_wh_sp_quantity: warehouse_space.quantity, wh_routing: wh_routing,
          selected_warehouse_space: warehouse_space
        ).perform
      end
    end
  end
end
