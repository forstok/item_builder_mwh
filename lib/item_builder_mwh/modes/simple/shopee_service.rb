# frozen_string_literal: true

require 'date'
require 'item_builder_mwh/modes.rb'
require 'item_builder_mwh/modes/simple/base'
class ItemBuilderMwh
  module Modes
    module Simple
      class ShopeeService < Base
        def perform
          {
            price: listing.price,
            sale_price: listing.sale_price,
            sale_start_at: listing.sale_start_at,
            sale_end_at: listing.sale_end_at,
            quantity: available_quantity,
            warehouse_id: wh_mapping(
              warehouse_space.warehouse_id
            )
          }
        end
      end
    end
  end
end
