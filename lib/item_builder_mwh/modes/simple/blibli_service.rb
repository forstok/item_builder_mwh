# frozen_string_literal: true

require 'date'
require 'item_builder_mwh/modes.rb'
require 'item_builder_mwh/modes/simple/base'
class ItemBuilderMwh
  module Modes
    module Simple
      class BlibliService < Base
        def perform
          {
            price: listing.price,
            sale_price: sale_price,
            sale_start_at: listing.sale_start_at,
            sale_end_at: listing.sale_end_at,
            quantity: local_variant,
            warehouse_id: wh_mapping(
              warehouse_space.warehouse_id
            )
          }
        end

        def local_variant
          qty = 0
          local_product['value']['items'].each do |item|
            qty = total_quantity(item) if item['itemSku'] == listing.local_id
          end
          qty
        end

        def total_quantity(item)
          # Make stock 0
          # if local reserved quantity is bigger than available quantity
          if item['reservedStockLevel2'] > available_quantity
            0
          else
            item['availableStockLevel2'] + item['reservedStockLevel2']
          end
        end

        # Find local product based on id
        # @return [Hash] Local Blibli product
        def local_product
          @local_product ||= JSON.parse(apigateway_post)
        end

        private

        def request
          {
            "gdnSku": listing.local_id
          }
        end

        def url
          url = ENV['API_GATEWAY_URL'] || 'apigateway.forstok.com'
          url + '/blibli/item'
        end

        def sale_price
          sale_price_policy.on_sale? ? listing.sale_price : listing.price
        end
      end
    end
  end
end
