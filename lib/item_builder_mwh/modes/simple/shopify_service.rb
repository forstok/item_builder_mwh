# frozen_string_literal: true

require 'date'
require 'item_builder_mwh/modes.rb'
require 'item_builder_mwh/modes/simple/base'
class ItemBuilderMwh
  module Modes
    module Simple
      class ShopifyService < Base
        def perform
          {
            price: listing.price,
            sale_price: sale_price,
            sale_start_at: listing.sale_start_at,
            sale_end_at: listing.sale_end_at,
            quantity: available_quantity,
            warehouse_id: wh_mapping(
              warehouse_space.warehouse_id
            )
          }
        end

        private

        def sale_price
          sale_price_policy.on_sale? ? listing.sale_price : listing.price
        end
      end
    end
  end
end
