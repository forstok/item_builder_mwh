# frozen_string_literal: true

require 'date'
require 'item_builder_mwh/modes.rb'
require 'item_builder_mwh/modes/simple_service'
require 'item_builder_mwh/modes/simple/sale_price_policy'
class ItemBuilderMwh
  module Modes
    module Simple
      class Base
        attr_reader :listing
        attr_reader :available_quantity

        def initialize(listing, available_quantity)
          raise 'listing is not set' if listing.nil?

          @listing = listing
          @available_quantity = available_quantity
        end

        def order_host
          url = ENV['ORDERS_URL'] || 'orders.forstok.com'
          url + '/api/v2/item_line/reserved_stock'
        end

        def reserved_params
          "account_id=#{listing.profile_channel_association_id}
          &item_variant_id=#{listing.variant_id}"
        end

        def reserved_stock
          RestClient.get("#{order_host}?#{reserved_params}")
        end

        def apigateway_get
          RestClient.get("#{host}?#{params}")
        end

        def headers
          {
            content_type: :json,
            accept: :json
          }
        end

        def credential
          account_id = listing.profile_channel_association_id
          host = ENV['CREDENTIAL_URL'] || 'user.forstok.com'
          RestClient.get("#{host}/credential?account_id=#{account_id}")
        end

        def data
          {
            "credential": JSON.parse(credential)['credential'],
            "data": request
          }
        end

        def api_data
          data.to_json
        end

        def sale_price_policy
          @sale_price_policy ||=
            ItemBuilderMwh::Modes::Simple::SalePricePolicy.new(listing: listing)
        end

        def apigateway_post
          RestClient.post(url, api_data, headers)
        end
      end
    end
  end
end
