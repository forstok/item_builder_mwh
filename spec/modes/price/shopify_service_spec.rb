# frozen_string_literal: true

require 'json'
require 'date'
require 'byebug'
RSpec.describe ItemBuilderMwh::Modes::Price::ShopifyService do
  subject { described_class.new(listing) }
  
  describe '#perform sale' do
    let(:sale_start_at) { DateTime.now }
    let(:sale_end_at) { DateTime.now + 1 }
    let(:listing) { double 'listing', id: 1, sku: 'sku', price: 10000, sale_price: 5000, sale_start_at: sale_start_at, sale_end_at: sale_end_at, profile_channel_association_id: 123, variant_id: 12 }
    let(:warehouse_spaces) { double 'warehouse spaces relation', id: 1, warehouse_id: 1, quantity: 10 }
    let(:warehouse) { double 'warehouse', id: 1, warehouse_id: 1, item_variant_id: 12, quantity: 10 }
    let(:warehouse_mapping) { double 'warehouse mapping relation' }
    let(:warehouses) { double 'warehouses', id: 1, warehouse_id: 1, active: true }
    let(:each) { [warehouses] }
    let(:response) {
      {
        price: 10000,
        sale_price: 5000,
        sale_start_at: sale_start_at,
        sale_end_at: sale_end_at
      }
    }
    it 'have to return correct hash' do
      allow(WarehouseSpace).to receive(:joins).with('JOIN warehouse_mappings ON
          warehouse_spaces.warehouse_id = warehouse_mappings.warehouse_id').and_return(warehouse_spaces)
      allow(warehouse_spaces).to receive(:where).with("warehouse_mappings.profile_channel_association_id=123").and_return(warehouse_spaces)
      allow(warehouse_spaces).to receive(:where).with("warehouse_spaces.item_variant_id=12").and_return(warehouse_spaces)
      allow(warehouse_spaces).to receive(:each).and_return(each)
      allow(WarehouseMapping).to receive(:where).with(profile_channel_association_id: listing.profile_channel_association_id, warehouse_id: warehouse.id).and_return(warehouse_mapping)
      allow(warehouse_mapping).to receive(:empty?).and_return(warehouse_mapping)
      expect(subject.perform).to be_eql(response)
    end
  end

  describe '#perform not sale' do
    let(:sale_start_at) { DateTime.now - 2 }
    let(:sale_end_at) { DateTime.now - 1 }
    let(:listing) { double 'listing', id: 1, sku: 'sku', price: 10000, sale_price: 5000, sale_start_at: sale_start_at, sale_end_at: sale_end_at, profile_channel_association_id: 123, variant_id: 12 }
    let(:warehouse_spaces) { double 'warehouse spaces relation', id: 1, warehouse_id: 1, quantity: 10 }
    let(:warehouse) { double 'warehouse', id: 1, warehouse_id: 1, item_variant_id: 12, quantity: 10 }
    let(:warehouse_mapping) { double 'warehouse mapping relation' }
    let(:warehouses) { double 'warehouses', id: 1, warehouse_id: 1, active: true }
    let(:each) { [warehouses] }
    let(:response) {
      {
        price: 10000,
        sale_price: 10000,
        sale_start_at: sale_start_at,
        sale_end_at: sale_end_at
      }
    }
    it 'have to return correct hash' do
      allow(WarehouseSpace).to receive(:joins).with('JOIN warehouse_mappings ON
          warehouse_spaces.warehouse_id = warehouse_mappings.warehouse_id').and_return(warehouse_spaces)
      allow(warehouse_spaces).to receive(:where).with("warehouse_mappings.profile_channel_association_id=123").and_return(warehouse_spaces)
      allow(warehouse_spaces).to receive(:where).with("warehouse_spaces.item_variant_id=12").and_return(warehouse_spaces)
      allow(warehouse_spaces).to receive(:each).and_return(each)
      allow(WarehouseMapping).to receive(:where).with(profile_channel_association_id: listing.profile_channel_association_id, warehouse_id: warehouse.id).and_return(warehouse_mapping)
      allow(warehouse_mapping).to receive(:empty?).and_return(warehouse_mapping)
      expect(subject.perform).to be_eql(response)
    end
  end
end
