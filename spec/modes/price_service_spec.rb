class WarehouseSpace;end
class WarehouseMapping;end
RSpec.describe ItemBuilderMwh::Modes::PriceService do
  subject { described_class.new(params) }
  let(:params){
    { listing: listing }
  }
  let(:listing) { double 'listing', id: 1, profile_channel_association_id: 123, sku: 'sku', local_id: 'local_id', local_item_id: 'local_item_id', price: 1230, sale_price: 123, sale_start_at: sale_start_at, sale_end_at: sale_end_at, variant_id: 12, channel_id: 15}
  let(:warehouse) { double 'warehouse', id: 1, warehouse_id: 1, item_variant_id: 12, quantity: 0 }
  let(:warehouse_spaces) { double 'warehouse spaces relation', id: 1, warehouse_id: 1 }
  let(:warehouse_mapping) { double 'warehouse mapping relation' }
  let(:warehouses) { double 'warehouses', id: 1, warehouse_id: 1, active: true }
  let(:each) { [warehouses] }
  let(:sale_start_at) { Time.now.to_s }
  let(:sale_end_at) { Time.now.to_s }

  describe '#perform' do
    it 'have to return correct hash' do
      allow(WarehouseSpace).to receive(:joins).with('JOIN warehouse_mappings ON
          warehouse_spaces.warehouse_id = warehouse_mappings.warehouse_id').and_return(warehouse_spaces)
      allow(warehouse_spaces).to receive(:where).with("warehouse_mappings.profile_channel_association_id=123").and_return(warehouse_spaces)
      allow(warehouse_spaces).to receive(:where).with("warehouse_spaces.item_variant_id=12").and_return(warehouse_spaces)
      allow(warehouse_spaces).to receive(:each).and_return(each)
      expect(subject.perform).to be_eql({:id=>1, :local_id=>"local_id", :local_item_id=>"local_item_id", :sku=>"sku", :warehouse=>[]})
    end
  end
  describe '#to_h' do
    it 'have to return correct hash' do
      allow(WarehouseSpace).to receive(:joins).with('JOIN warehouse_mappings ON
          warehouse_spaces.warehouse_id = warehouse_mappings.warehouse_id').and_return(warehouse_spaces)
      allow(warehouse_spaces).to receive(:where).with("warehouse_mappings.profile_channel_association_id=123").and_return(warehouse_spaces)
      allow(warehouse_spaces).to receive(:where).with("warehouse_spaces.item_variant_id=12").and_return(warehouse_spaces)
      allow(warehouse_spaces).to receive(:each).and_return(each)
      allow(WarehouseMapping).to receive(:where).with(profile_channel_association_id: listing.profile_channel_association_id, warehouse_id: warehouse.id).and_return(warehouse_mapping)
      allow(warehouse_mapping).to receive(:empty?).and_return(warehouse_mapping)
      expect(subject.to_h(warehouse_spaces)).to be_eql({:price=>1230, :sale_end_at=>sale_end_at, :sale_price=>123, :sale_start_at=>sale_start_at, :warehouse_id=>nil})
    end
  end
end

