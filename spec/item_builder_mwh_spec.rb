# frozen_string_literal: true

class VariantListing;end
class WarehouseSpace;end
RSpec.describe ItemBuilderMwh do
  subject { described_class }

  describe '#build' do
    let(:listing) { double 'listing', id: 1, sku: 'sku', local_id: 'local_id', variant_id: 12, local_item_id: 'local_item_id', price: 1230, sale_price: 123, sale_start_at: sale_start_at, sale_end_at:    sale_end_at, profile_channel_association_id: 1234}
    let(:warehouse) { double 'warehouse', id: 1, warehouse_id: 1, item_variant_id: 12, quantity: 0 }
    let(:warehouse_spaces) { double 'warehouse spaces relation', id: 1, warehouse_id: 1, quantity: 10 }
    let(:warehouse_mapping) { double 'warehouse mapping relation' }
    let(:warehouses) { double 'warehouses', id: 1, warehouse_id: 1, active: true }
    let(:each) { [warehouses] }
    let(:sale_start_at) { Time.now.to_s }
    let(:sale_end_at) { Time.now.to_s }
    before do
    end
    context 'when mode is simple' do
      it 'return correct response' do
        allow(VariantListing).to receive(:where).with(id: [12]).and_return([listing])
        allow(WarehouseSpace).to receive(:joins).with('JOIN warehouse_mappings ON
          warehouse_spaces.warehouse_id = warehouse_mappings.warehouse_id').and_return(warehouse_spaces)
        allow(warehouse_spaces).to receive(:where).with("warehouse_mappings.profile_channel_association_id=1234").and_return(warehouse_spaces)
        allow(warehouse_spaces).to receive(:where).with("warehouse_spaces.item_variant_id=12").and_return(warehouse_spaces)
        allow(warehouse_spaces).to receive(:each).and_return(each)
        expect(ItemBuilderMwh.build([12], :simple)).to be_eql([{:id=>1, :local_id=>"local_id", local_item_id: 'local_item_id', :sku=>"sku", :warehouse=>[]}])
      end
    end

    context 'when mode is price' do
      it 'return correct response' do
        allow(VariantListing).to receive(:where).with(id: [12]).and_return([listing])
        allow(WarehouseSpace).to receive(:joins).with('JOIN warehouse_mappings ON
          warehouse_spaces.warehouse_id = warehouse_mappings.warehouse_id').and_return(warehouse_spaces)
        allow(warehouse_spaces).to receive(:where).with("warehouse_mappings.profile_channel_association_id=1234").and_return(warehouse_spaces)
        allow(warehouse_spaces).to receive(:where).with("warehouse_spaces.item_variant_id=12").and_return(warehouse_spaces)
        allow(warehouse_spaces).to receive(:each).and_return(each)
        expect(ItemBuilderMwh.build([12], :price)).to be_eql([{:id=>1, :local_id=>"local_id", local_item_id: 'local_item_id', :sku=>"sku", :warehouse=>[]}])
      end
    end
    context 'when mode is quantity' do
      it 'return correct response' do
        allow(VariantListing).to receive(:where).with(id: [12]).and_return([listing])
        allow(WarehouseSpace).to receive(:joins).with('JOIN warehouse_mappings ON
          warehouse_spaces.warehouse_id = warehouse_mappings.warehouse_id').and_return(warehouse_spaces)
        allow(warehouse_spaces).to receive(:where).with("warehouse_mappings.profile_channel_association_id=1234").and_return(warehouse_spaces)
        allow(warehouse_spaces).to receive(:where).with("warehouse_spaces.item_variant_id=12").and_return(warehouse_spaces)
        allow(warehouse_spaces).to receive(:each).and_return(each)
        expect(ItemBuilderMwh.build([12], :quantity)).to be_eql([{:id=>1, :local_id=>"local_id", local_item_id: 'local_item_id', :sku=>"sku", :warehouse=>[]}])
      end
    end
    context 'when mode is active' do
      it 'return correct response' do
        allow(VariantListing).to receive(:where).with(id: [12]).and_return([listing])
        allow(WarehouseSpace).to receive(:joins).with('JOIN warehouse_mappings ON
          warehouse_spaces.warehouse_id = warehouse_mappings.warehouse_id').and_return(warehouse_spaces)
        allow(warehouse_spaces).to receive(:where).with("warehouse_mappings.profile_channel_association_id=1234").and_return(warehouse_spaces)
        allow(warehouse_spaces).to receive(:where).with("warehouse_spaces.item_variant_id=12").and_return(warehouse_spaces)
        allow(warehouse_spaces).to receive(:each).and_return(each)
        expect(ItemBuilderMwh.build([12], :active)).to be_eql([{:id=>1, :local_id=>"local_id", local_item_id: 'local_item_id', :sku=>"sku", :warehouse=>[]}])
      end
    end
  end
end
